package model.vo;

public class Servicio implements Comparable<Servicio>{

	
	private String trip_id;
	private String taxi_id;
	private String trip_miles;
	private String trip_seconds;
	private String trip_total;
	private String trip_start_timestamp;
	private String pickup_community_area;

	
        public String getTripId() {
            // TODO Auto-generated method stub
            return trip_id;
        }

        /**
         * @return id - Taxi_id
         */
        public String getTaxiId() {
            // TODO Auto-generated method stub
            return taxi_id;
        }

        /**
         * @return time - Time of the trip in seconds.
         */
        public int getTripSeconds() {
            // TODO Auto-generated method stub
            return 0;
        }

        /**
         * @return miles - Distance of the trip in miles.
         */
        public double getTripMiles() {
            // TODO Auto-generated method stub
            return 0;
        }

        /**
         * @return total - Total cost of the trip
         */
        public double getTripTotal() {
            // TODO Auto-generated method stub
            return 0;
        }

        public String getStartTime(){
            //TODO Auto-generated method stub
            return "hora";
        }

        public String getPickupZone(){
            //TODO Auto-generated method stub
            return pickup_community_area;
        }

        public int getDropOffZone(){
            //TODO Auto-generated method stub
            return -1;
        }


        public double getPickupLatitud(){
            //TODO Auto-generated method stub
            return -1;
        }

        public double getPickupLongitud(){
            //TODO Auto-generated method stub
            return -1;
        }

        @Override
        public int compareTo(Servicio arg0) {
            // TODO Auto-generated method stub
            return this.getTripId().compareTo( arg0.getTripId() );
        }
}