package model.vo;

import model.data_structures.Lista;

public class Compania implements Comparable<Compania> {

	private String company;
	private Lista<String> ids_taxis;



	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return this.getCompany().compareTo(o.getCompany());
	}



	public String getCompany() {
		return company;
	}



	public void setCompany(String company) {
		this.company = company;
	}



	public Lista<String> getIds_taxis() {
		return ids_taxis;
	}



	public void setIds_taxis(Lista<String> ids_taxis) {
		this.ids_taxis = ids_taxis;
	}
}
