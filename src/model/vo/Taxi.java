package model.vo;

public class Taxi implements Comparable<Taxi> {
	private String taxi_id;
	private String company;
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		// TODO Auto-generated method stub
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public int compareTo(Taxi o)
	{
		// TODO Auto-generated method stub
		return this.getTaxiId().compareTo(o.getTaxiId());
	}
}
