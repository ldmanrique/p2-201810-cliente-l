package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.sun.org.apache.bcel.internal.generic.NEW;

import API.ITaxiTripsManager;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.MySeparateChainingHash;
import model.data_structures.MyTree23;
import model.vo.Compania;

import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";



	DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss" );
	MyTree23<String, Lista> companias_taxis= new MyTree23<String, Lista>();
	Lista<Taxi> taxis= new Lista<Taxi>();
	MySeparateChainingHash<String, Lista> areas_servicios = new MySeparateChainingHash<String, Lista>();
	Lista<Servicio> servicios= new Lista<Servicio>();

	@Override
	public boolean cargarSistema(String direccionJson) 
	{
		// TODO Auto-generated method stub
		try
		{
			FileInputStream stream = new FileInputStream(new File(direccionJson));
			JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
			Gson gson = new GsonBuilder().create();
			// Read file in stream mode
			reader.beginArray();
			while (reader.hasNext()) 
			{
				// Read data into object model
				Taxi t = gson.fromJson(reader, Taxi.class);
				Servicio s = gson.fromJson(reader, Servicio.class);
				if (taxis.get(t)==null) 
				{
					taxis.add(t);
					Lista<String> taxisids= new Lista<String>();
					taxisids.add(t.getTaxiId());
					if(companias_taxis.buscar(t.getCompany())==null)
					{
						companias_taxis.insertar(t.getCompany(), taxisids);
					}
					else
					{
						companias_taxis.buscar(t.getCompany()).setVal(taxisids);
					}
				}
				if(servicios.get(s)==null)
				{
					servicios.add(s);
					Lista<Servicio> services =new Lista<Servicio>();
					services.add(s);
					if(areas_servicios.get(s.getPickupZone())==null)
					{
						areas_servicios.put(s.getPickupZone(), services);
					}
					else
					{
						areas_servicios.get(s.getPickupZone()).add(s);
					}
				}
				A2ServiciosPorDuracion(s.getTripSeconds());
				B2ServiciosPorZonaRecogidaYLlegada(s.getPickupZone(), s.getDropOffZone(), s.getStartTime(), s.g, horaI, horaF)
				break;
			}
			reader.close();
			return true;
		}catch(Exception e)
		{
			System.out.println("Falla la carga");

			return false;
		}
	}


	@Override
	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		// TODO Auto-generated method stub
		Lista<TaxiConServicios> l= new Lista<TaxiConServicios>();
		Lista <Servicio> s= areas_servicios.get(""+zonaInicio);

		String mas= null;
		String temp=null;
		int cont=0;
		while(s.iterator().hasNext())
		{
			temp= s.iterator().next().getTaxiId();
			int n=0;
			while(s.iterator().hasNext()) {
				if(s.iterator().next().equals(temp))
				{
					n++;
				}
				if(n>=cont)
				{
					mas=temp;
					cont=n;

				}
				if(n>cont)
				{
					l=null;
				}
				TaxiConServicios t= new TaxiConServicios(mas, compania);

				l.add(t);
			}
		}

		return l;
	}


	@Override
	public IList<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub
		Lista<Servicio> re= new Lista<Servicio>();
		if(duracion!=0){
		int n=duracion/60;
		}
		MySeparateChainingHash<String, Lista<Servicio>> h= new MySeparateChainingHash<>();
	Lista<Servicio> aux= servicios;
	Servicio a= aux.iterator().next();
		
		
		
		return re;
	}


	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}

	@Override
	public IList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}




}
