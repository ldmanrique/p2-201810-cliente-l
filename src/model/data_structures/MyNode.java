package model.data_structures;

import java.io.Serializable;
import java.util.Iterator;


public class MyNode <Key extends Comparable<? super Key>, Value> implements Serializable{
	// -----------------------------------------------------------------
	// Constantes
	// -----------------------------------------------------------------

	/**
	 * Constante para la serializaci�n 
	 */
	private static final long serialVersionUID = 1L;

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------
	private Value val;
	private Key k;
	/**
	 * Ra�z izquierda del nodo
	 */
	private MyNode<Key, Value> raizIzq;

	/**
	 * Ra�z derecha del nodo
	 */
	private MyNode<Key, Value> raizDer;

	/**
	 * Sub�rbol izquierdo
	 */
	private MyNode<Key, Value> hijoIzq;

	/**
	 * Sub�rbol central
	 */
	private MyNode<Key, Value> hijoCent;

	/**
	 * Sub�rbol derecho
	 */
	private MyNode<Key, Value> hijoDer;

	// -----------------------------------------------------------------
	// Constructores
	// -----------------------------------------------------------------

	/**
	 * Constructor del nodo con el primer elemento. <br>
	 * <b> post: </b> Se construy� el nodo con el elemento especificado.
	 * @param obj Elemento a agregar al nodo
	 */
	public MyNode( Key obj , Value v)
	{
		setVal(v);
		setK(obj);
		raizIzq = null;
		raizDer = null;
		hijoIzq = null;
		hijoCent = null;
		hijoDer = null;
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Devuelve la ra�z izquierda del nodo. <br>
	 * <b>post: </b> Se retorn� la ra�z izquierda del nodo.
	 * @return Ra�z izquierda del nodo.
	 */
	public MyNode<Key, Value> darRaizIzq( )
	{
		return raizIzq;
	}

	/**
	 * Devuelve la ra�z derecha del nodo. <br>
	 * <b>post: </b> Se retorn� la ra�z derecha del nodo.
	 * @return Ra�z derecha del nodo
	 */
	public MyNode darRaizDer( )
	{
		return raizDer;
	}

	/**
	 * Devuelve el hijo izquierdo del nodo.<br>
	 * <b>post: </b> Se retorn� el hijo izquierdo del nodo.
	 * @return Hijo izquierdo del nodo
	 */
	public MyNode<Key, Value> darHijoIzq( )
	{
		return hijoIzq;
	}

	/**
	 * Devuelve el Hijo central del nodo. <br>
	 * <b>post: </b> Se retorn� el hijo central del nodo. <br>
	 * @return Hijo central del nodo
	 */
	public MyNode<Key, Value> darHijoCent( )
	{
		return hijoCent;
	}

	/**
	 * Devuelve el Hijo derecho del nodo. <br>
	 * <b>post: </b> Se retorn� el hijo 3 del nodo. <br>
	 * @return Hijo 3 del nodo
	 */
	public MyNode<Key, Value> darHijoDer( )
	{
		return hijoDer;
	}

	/**
	 * Indica si el nodo es una hoja.<br>
	 * <b>post: </b> Se retorn� true si el nodo es una hoja o false de lo contrario. Un nodo es una hoja si sus tres hijos se encuentran en null.
	 * @return True si es hoja, False si no
	 */
	public boolean esHoja( )
	{
		return hijoIzq == null && hijoCent == null && hijoDer == null;
	}

	/**
	 * Informa si un elemento se encuentra presente en el �rbol 1-2-3 cuya ra�z es el nodo actual. <br>
	 * <b>pre: </b> modelo!=null. <br>
	 * <b>post: </b> Se retorno un elemento que corresponde al modelo dado. Si ning�n elemento corresponde al elemento se retorna null.
	 * 
	 * @param modelo Modelo del elemento que se desea buscar
	 * @return Elemento encontrado o null si no lo encuentra
	 */
	public MyNode<Key, Value> buscar( Key modelo )
	{
		//
		// Compara con el primer elemento
		int resultado1 = modelo.compareTo( raizIzq.k );
		if( resultado1 == 0 )
		{
			return raizIzq;
		}
		else if( resultado1 < 0 )
		{
			return ( hijoIzq == null ) ? null : hijoIzq.buscar( modelo );
		}
		else if( raizDer == null )
		{
			return ( hijoCent == null ) ? null : hijoCent.buscar( modelo );
		}
		else
		{
			// Compara con el segundo elemento
			int resultado2 = modelo.compareTo( raizDer.k );
			if( resultado2 == 0 )
			{
				return raizDer;
			}
			else if( resultado2 < 0 )
			{
				return ( hijoCent == null ) ? null : hijoCent.buscar( modelo );
			}
			else
			{
				return ( hijoDer == null ) ? null : hijoDer.buscar( modelo );
			}
		}
	}

	/**
	 * Calcula la altura del �rbol 1-2-3 cuya ra�z es este nodo. <br>
	 * <b>post: </b> Se retorn� la altura del �rbol.
	 * @return Devuelve la altura del �rbol
	 */
	public int darAltura( )
	{
		if( esHoja( ) )
		{
			return 1;
		}
		else
		{
			return hijoIzq != null ? hijoIzq.darAltura( ) + 1 : hijoCent != null ? hijoCent.darAltura( ) + 1 : hijoDer != null ? hijoDer.darAltura( ) + 1 : 1;
		}
	}

	/**
	 * Agrega un nuevo elemento al �rbol 1-2-3 cuya ra�z el nodo actual y retorna el nodo que corresponde a la nueva ra�z del �rbol<br>
	 * <b>pre: </b> obj es diferente de null. <br>
	 * <b>pre: </b> obj!=null. <br>
	 * <b>post: </b> Se insert� un elemento en el �rbol si este no exist�a previamente en la estructura.
	 * 
	 * @param obj != null
	 * @throws ElementoExisteException El elemento ya existe en el �rbol
	 */
	public void insertar( Key elemento , Value v) throws Exception
	{
		// Compara el elemento a insertar con las raices del nodo
		int menorIzq = elemento.compareTo( raizIzq.k );
		int menorDer = raizDer != null ? elemento.compareTo( raizDer.k ) : 0;
		// Se busca si el elemento se encuentra en el nodo
		if( menorIzq == 0 || ( raizDer != null && menorDer == 0 ) )
			throw new Exception( "El elemento ya existe en el �rbol" );

		// Si el nodo no tiene ra�z derecha y el elemento es mayor que la raiz izquierda
		if( raizDer == null && menorIzq > 0 )
		{
			raizDer.setK(elemento);
		}
		else if( raizDer == null && menorIzq < 0 )
		{
			// El nodo no tiene raiz derecha y el elemento a agregar es menor que el que se encuentra en la raiz izquierda
			//Si el hijo izquierdo es null
			if(hijoIzq == null)
			{
				raizDer = raizIzq;
				raizIzq.setK(elemento);
			}
			else
				hijoIzq.insertar(elemento, v);
		}

		if( menorIzq < 0 && menorDer < 0 )
		{
			// El elemento es menor que la raiz izquierda y que la raiz derecha
			if( hijoIzq == null )
			{
				hijoIzq = new MyNode<Key, Value>( elemento, v );
			}
			else
			{
				hijoIzq.insertar( elemento, v );
			}
		}
		else if( menorIzq > 0 && menorDer < 0 )
		{
			// El elemento es mayor que la raiz izquierda y menor que la raiz derecha
			if( hijoCent == null )
			{
				hijoCent = new MyNode<Key, Value>( elemento, v );
			}
			else
			{
				hijoCent.insertar( elemento, v );
			}
		}
		else if( menorIzq > 0 && menorDer > 0 )
		{
			// El elemento es mayor que la raiz izquierda y que la raiz derecha
			if( hijoDer == null )
			{
				hijoDer = new MyNode<Key, Value>( elemento, v );
			}
			else
			{
				hijoDer.insertar( elemento, v );
			}
		}
	}

	/**
	 * Elimina un valor dado del �rbol 1-2-3 cuya ra�z es este nodo, y retorna una referencia al nodo ra�z de la estructura resultante. <br>
	 * <b>pre: </b> obj!=null. <br>
	 * <b>post: </b> Se elimin� un elemento del �rbol si este exist�a en la estructura.
	 * 
	 * @param obj El objeto a ser eliminado
	 * @throws ElementoNoExisteException Excepci�n generada si el elemento especificado no existe
	 */
	public MyNode<Key, Value> eliminar( Key elemento ) throws Exception
	{
		int mayorIzq = elemento.compareTo( raizIzq.k );
		int mayorDer = raizDer != null ? elemento.compareTo( raizDer.k ) : 0;

		if( mayorIzq == 0 )
		{
			// El elemento a eliminar se encuentra en la ra�z izquierda del nodo
			if( hijoIzq != null )
			{
				// El nodo tiene hijo izquierdo y es necesario buscar un nuevo valor para la ra�z izquierda
				raizIzq = hijoIzq.calcularMayorElem( );
				hijoIzq = hijoIzq.eliminar( raizIzq.k );
				return this;
			}
			else if( raizDer != null )
			{
				// El nodo no tiene hijo izquierdo entonces toca colocar la ra�z derecha en la ra�z izquierda
				raizIzq = raizDer;
				raizDer = hijoDer != null ? hijoDer.calcularMayorElem( ) : null;
				if( raizDer != null )
					hijoDer = hijoDer.eliminar( raizDer.k );
				// Es necesario reacomodar los hijos para que cumpla las reglas del �rbol 1-2-3
				hijoIzq = hijoCent;
				hijoCent = hijoDer;
				hijoDer = null;
				return this;
			}
			else
			{
				if(hijoCent != null)
				{
					return hijoCent;
				}
				return null;
			}
		}
		else if( mayorDer == 0 && raizDer != null )
		{
			// El valor a eliminar se encuentra en la ra�z derecha del nodo
			if( hijoDer != null )
			{
				// Si el nodo tiene hijo derecho toca colocar una nueva raiz derecha
				raizDer = hijoDer.calcularMenorElem( );
				hijoDer = hijoDer.eliminar( raizDer.k );
				return this;
			}
			else
			{ 
				// Si no tiene hijo derecho ni central solo se elimina la ra�z 
				// Si tiene hijo central se calcula el mayor del hijo central y se deja como ra�z, eliminando �ste
				// del hijo central
				if(hijoCent == null)
					raizDer = null;
				else
				{
					raizDer = hijoCent.calcularMayorElem();
					hijoCent = hijoCent.eliminar(raizDer.k);
				}

				return this; 
			}
		}

		if( mayorIzq < 0 && hijoIzq != null )
		{
			// El elemento es menor que la ra�z izquierda y que la ra�z derecha
			hijoIzq = hijoIzq.eliminar( elemento );
		}
		else if( ( raizDer == null || mayorDer < 0 ) && hijoCent != null )
		{
			// El elemento es mayor que la ra�z izquierda y menor que la ra�z derecha
			hijoCent = hijoCent.eliminar( elemento );
		}
		else if( hijoDer != null )
		{
			// El elemento es mayor que la ra�z izquierda y que la ra�z derecha
			hijoDer = hijoDer.eliminar( elemento );
		}
		else
		{
			throw new Exception( "No se encontr� el elemento a eliminar" );
		}
		return this;
	}

	/**
	 * Devuelve los elementos del �rbol en inorden. <br>
	 * <b>pre: </b> resultado!=null. <br>
	 * <b>post: </b> Se retorno un vector con el recorrido en inorden del �rbol.
	 * 
	 * @param resultado Vector con los elementos del �rbol en inorden
	 */
	public void inorden( Lista resultado ) throws Exception
	{
		if( hijoIzq != null )
		{
			hijoIzq.inorden( resultado );
		}
		resultado.add( raizIzq.k );
		if( hijoCent != null )
		{
			hijoCent.inorden( resultado );
		}
		if( raizDer != null )
		{
			resultado.add( raizDer.k );
			if( hijoDer != null )
			{
				hijoDer.inorden( resultado );

			}
		}
	}

	// -----------------------------------------------------------------
	// Operaciones Auxiliares
	// -----------------------------------------------------------------

	/**
	 * Retorna el menor elemento que tiene el �rbol cuya ra�z es el nodo actual. <b>post: </b> Se retorno el menor elemento cuya ra�z es el nodo actual.
	 * @return Valor que representa el menor de los elementos
	 */
	/**
	 * Retorna el menor elemento del �rbol 1-2-3 cuya ra�z es este nodo. <br>
	 * <b>post: </b> Se retorn� el menor elemento del �rbol cuya ra�z es este nodo.
	 * 
	 * @return Menor elemento del �rbol
	 */
	private MyNode<Key, Value> calcularMenorElem( )
	{
		MyNode<Key, Value> aux = this;
		while( aux.hijoIzq != null )
			aux = aux.hijoIzq;
		return aux.raizIzq;
	}

	/**
	 * Retorna el mayor elemento que tiene el �rbol cuya ra�z es el nodo actual. <b>post: </b> Se retorno el mayor elemento cuya ra�z es el nodo actual.
	 * @return Valor que representa el mayor de los elementos
	 */
	private MyNode<Key, Value> calcularMayorElem( )
	{
		if(raizDer == null)
		{
			if(hijoCent == null)
			{
				return raizIzq;
			}
			else
				return hijoCent.calcularMayorElem();
		}
		else
		{
			if(hijoDer == null)
				return raizDer;
			else
				return hijoDer.calcularMayorElem();
		}

	}

	public Value getVal() {
		return val;
	}

	public void setVal(Value val) {
		this.val = val;
	}

	public Key getK() {
		return k;
	}

	public void setK(Key k) {
		this.k = k;
	}



}
