package model.data_structures;

import java.io.Serializable;
import java.util.Iterator;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;

public class MyTree23 <Key extends Comparable<? super Key>, Value> implements Serializable, IMyTree23<Key, Value>
{

	// -----------------------------------------------------------------
	// Constantes
	// -----------------------------------------------------------------

	/**
	 * Constante para la serializaci�n 
	 */
	private static final long serialVersionUID = 1L;

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------
	/**
	 * Ra�z del �rbol 1-2-3
	 */
	private MyNode<Key, Value> raiz;

	/**
	 * Peso del �rbol 1-2-3
	 */
	private int peso;

	// -----------------------------------------------------------------
	// Constructores
	// -----------------------------------------------------------------

	/**
	 * Construye un nuevo �rbol vac�o. <br>
	 * <b>post: </b> Se construy� un �rbol vac�o, con ra�z null.
	 */
	public MyTree23( )
	{
		raiz = null;
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Devuelve la ra�z del �rbol para ser navegada. <br>
	 * <b>post: </b> Se retorn� la ra�z del �rbol.
	 * @return Ra�z del �rbol
	 */
	public MyNode<Key, Value> darRaiz( )
	{
		return raiz;
	}


	/* (non-Javadoc)
	 * @see uniandes.cupi2.collections.arbol.IArbol#buscar(java.lang.Object)
	 */
	public MyNode buscar( Key modelo )
	{
		if (raiz != null)
			return raiz.buscar( modelo );
		else
			return null;
	}


	/* (non-Javadoc)
	 * @see uniandes.cupi2.collections.arbol.IArbol#darAltura()
	 */
	public int darAltura( )
	{
		return raiz == null ? 0 : raiz.darAltura( );
	}


	/* (non-Javadoc)
	 * @see uniandes.cupi2.collections.arbol.IArbol#darPeso()
	 */
	public int darPeso( )
	{
		return peso;
	}


	/* (non-Javadoc)
	 * @see uniandes.cupi2.collections.arbol.IArbolOrdenado#insertar(java.lang.Comparable)
	 */
	public void insertar( Key elemento, Value val ) throws Exception
	{
		if( raiz == null )
		{
			// Caso 1: el �rbol es vac�o
			raiz = new MyNode<Key, Value>( elemento,val );
		}
		else
		{
			// Caso 2: el �rbol no es vac�o
			raiz.insertar( elemento, val );
		}
		peso++;
	}


	/* (non-Javadoc)
	 * @see uniandes.cupi2.collections.arbol.IArbolOrdenado#eliminar(java.lang.Comparable)
	 */
	public void eliminar( Key elemento ) throws Exception
	{
		if( raiz != null )
		{
			// Caso 1: el �rbol no es vac�o
			raiz = raiz.eliminar( elemento );
			peso--;
		}
		else
		{
			// Caso 2: el �rbol es vac�o
			throw new Exception( "El elemento especificado no existe en el �rbol" );
		}
	}

	/**
	 * Devuelve los elementos del �rbol en inorden. <br>
	 * <b>post: </b> Se retorno el iterador para recorrer los elementos del �rbol en inorden.
	 * @return Iterador para recorrer los elementos del �rbol en inorden. Diferente de null.
	 */
	public Lista inorden( )
	{
		Lista resultado = new Lista();
		if( raiz != null )
		{
			try
			{
				raiz.inorden( resultado );
			}
			catch( Exception e )
			{
				// Nunca deber�a lanzar esta excepci�n porque el tama�o del
				// iterador es el peso del �rbol
			}
		}
		return resultado;
	}


}
